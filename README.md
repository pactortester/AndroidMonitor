# AndroidMonitor

#### 介绍
基于Python结合InfluxDB及Grafana图表实时监控Android系统和应用进程


#### 使用说明
参考wiki [基于Python结合InfluxDB及Grafana图表实时监控Android系统和应用进程](https://gitee.com/ishouke/AndroidMonitor/wikis/%E5%9F%BA%E4%BA%8EPython%E7%BB%93%E5%90%88InfluxDB%E5%8F%8AGrafana%E5%9B%BE%E8%A1%A8%E5%AE%9E%E6%97%B6%E7%9B%91%E6%8E%A7Android%E7%B3%BB%E7%BB%9F%E5%92%8C%E5%BA%94%E7%94%A8%E8%BF%9B%E7%A8%8B?sort_id=1341682)


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request